import './box.css';
import Confirm_order from './Confirm_order';
import item1 from '../Assets/item1.jpg';
import App from '../App';
import { Link, useParams } from 'react-router-dom';
import useFetch from '../useFetch';


const Item = ({item}) => {
    const {Item}= useFetch('http://localhost:8000/items/');
    Item && console.log(Item)
    return (
        <div className="col-sm-6 col-md-5 col-lg-4 item">
            <div className="box">
                <img className="rounded img-fluid menu-img" src={item.img} alt={item.name}/>
                <h3 className="name">{item.name}</h3>
                <p className="description">{item.desc}</p>
                <p className="description">Preparation Time:{item.preparation_time}hrs</p>
                <div className="d-flex justify-content-around align-items-center">
                    <Link to={`/confirm_order/${item.id}`}>
                    <button  id="order" className="btn btn-success"type="button" style={{borderRadius:"20px"}} >
                        Order
                    </button>
                    </Link>
                    <span id ="price" className="badge rounded-pill price">{item.price} Rs</span>
                </div>
            </div>
        </div>
    );
};

export default Item;