import React from "react";
import "./Footer.css";

function Footer()
{
    return (
        <div className="container-footer">
            <div className="row">
                
                <div className="col" id='col1'>
                    <h5>About Us</h5>
                    <p>
                        Our Menu highlights things that utilization the sound and fragrant flavors, however, forgets
                        the stuffing ghee, spread, oil and overwhelming cream.
                    </p>
                </div>


                <div className="col" id="col2">
                    <h5>Address</h5>
                    <p>
                        Ganesha Complex, 271, 271, Outer Ring Road BTM Layout, Bengaluru, Karnataka 560068
                    </p>
                </div>


                <div className="col" id='col3'>
                    <h5>Contact</h5>
                    <p>
                        Rohith N
                        Ph-9844514213
                        Email-admin123@admin.com

                    </p>
                </div>



            </div>
        </div>
    );
}

export default Footer