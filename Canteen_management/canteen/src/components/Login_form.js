import React, { useState } from 'react'
import "../index.css"

function Login_form({Login,error}) {
  const[info, setInfo]=useState({ email:"",password:""});

  const submitHandler = e =>{
    e.preventDefault();

    Login(info);
  }
  return (




    <form onSubmit={submitHandler}>

    <div className='form-inner'>
      <h2>Login</h2>
      
      
        <p>{error}</p>
        
      <div className='form-group'>
      <label htmlFor='email'>Email</label>
      <input type="email" name='email' id='email' onChange={e=>setInfo({...info, email:e.target.value})} value={info.email}/>
      </div>

      <div className='form-group'>
    <label htmlFor='password'>Password</label>
      <input type="password" name='password' id='password' onChange={e=>setInfo({...info, password:e.target.value})} value={info.password}/>
      </div>

      <div>
        <input type="submit" value="Login"/>
      </div>


    </div>

    </form>
    
  )
}

export default Login_form