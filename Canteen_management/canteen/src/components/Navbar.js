import React from "react";
import "./Navbar.css";
import {BrowserRouter as Router,Link, Route, Switch} from 'react-router-dom';
import Home from "../Home";
import useFetch from "../useFetch";

function Navbar()
{

    return (
        
        
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
          <Link to="/menu">
        <a class="nav-link"  aria-current="page" >Menu</a>
        </Link>
        <Link to="/active_order">
        <a class="nav-link" >Active Order</a>
        </Link>
        <Link to="/order_history">
        <a class="nav-link" >Order History</a>
        </Link>
        <Link to="/invoice">
        <a class="nav-link" href="_">Invoice</a>
        </Link>
         
      </div>
    </div>
  </div>
</nav>
  );
}

export default Navbar


/*const {body}=useFetch("http://localhost:8000/items")
       
       
       console.log(body)
fetch('http://localhost:8000/items',{
           method:'POST',
            body:JSON.stringify(body),
        
       })*/