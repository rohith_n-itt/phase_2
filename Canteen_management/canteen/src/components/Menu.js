import React, { useState } from 'react'
import Navbar from './Navbar'
import Footer from './Footer'

import Item from './Item'
import './Menu.css'
import './box.css';
import useFetch from '../useFetch'


const Menu = () => {

    const name="Menu" 
 const desc="Tasty food made with fresh ingredients"

    const {Items}=useFetch("http://localhost:8000/items");

    return (
        <div>
            <Navbar/>
         <section className="features-boxed">
            <div className="container">
                <div className="intro">
                    <h2 className="text-center">{name}</h2>
                    <p className="text-center">{desc}</p>
                </div>
                <div className="row justify-content-center features">
                     {Items && Items.map((item) => (
                        <Item  item={item}/>
                    ))}
                </div>
            </div>
        </section>
        <Footer/>
        </div>

    
    );
};

export default Menu;









