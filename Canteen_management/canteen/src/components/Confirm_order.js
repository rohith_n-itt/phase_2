import React, { useEffect,useState } from 'react';
import { useParams } from 'react-router-dom';
import useFetch from '../useFetch';
import Navbar from '../components/Navbar.js';
import Footer from '../components/Footer.js';
import Item_order from './Item_order';
function Confirm_order() {

  const {id}=useParams();
  const {Items}= useFetch('http://localhost:8000/items');
  let Item=null;

  Items && Items.map((item_d)=>{
    if(item_d.id==id)
    {
    Item=item_d;
    }
});


  
  return (
    
    <div>
      <Navbar/>

      {Item && <Item_order Item={Item}/>}

    <Footer/>
    </div>
  );
}

export default Confirm_order;