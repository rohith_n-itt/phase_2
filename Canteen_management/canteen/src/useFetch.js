import { useState,useEffect } from "react";

const  useFetch=(url)=>{

    const[Items,setItems]=useState(null);
    useEffect( ()=>{
        
        fetch(url)
        .then(res=>{
          return res.json();
        }) 
        .then(data=>{
          setItems(data);
          })
      },[url]);

      if(Items)     
  return {Items}
  
    return<div></div>
}

export default useFetch;