import React,{ useEffect, useState} from 'react';
import Login_form from './components/Login_form.js';

import Menu  from './components/Menu.js';
import Navbar from './components/Navbar.js';

import "./App.css"
import "./components/Footer.css"
import Home from './Home.js';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Confirm_order from './components/Confirm_order.js';

function App() {  

  return(
    <Router>
<Switch>
            <Route exact path="/">
                    <Home/>
                </Route>

                <Route exact path="/navbar">
                    <Navbar/>
                </Route>
                <Route exact path="/confirm_order">
                    <Confirm_order/>
                </Route>
                <Route exact path="/confirm_order/:id">
                    <Confirm_order/>
                </Route>
                <Route exact path="/menu">
                    <Menu/>
                </Route>


            </Switch>
    

    /</Router>
  )
 
  
}

export default App;
